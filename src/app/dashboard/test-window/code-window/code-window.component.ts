import { Component, OnInit } from '@angular/core';


enum Code {
  Selenium,
  JUnit,
  TestNG,
}

class CodeWindow {
  constructor(public code: Code, public name: string) {
  }
}

@Component({
  selector: 'app-code-window',
  templateUrl: './code-window.component.html',
  styleUrls: ['./code-window.component.css']
})
export class CodeWindowComponent implements OnInit {
  codeWindows = [
    new CodeWindow(Code.Selenium, 'Selenium'),
    new CodeWindow(Code.JUnit, 'JUnit'),
    new CodeWindow(Code.TestNG, 'TestNG')
  ];

  constructor() { }

  parseCode(code: Code): string {
    switch (code) {
      case Code.Selenium:
        return this.parseSelenium();
      case Code.JUnit:
        return this.parseJUnit();
      case Code.TestNG:
        return this.parseTestNg();
    }
    return this.parseSelenium();
  }


  parseSelenium(): string {
    return 'sample selenium';
  }

  parseJUnit(): string {
    return 'sample Junit';
  }

  parseTestNg(): string {
    return 'sample TestNG';
  }

  ngOnInit() {
  }

}
